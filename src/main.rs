#![no_std]
#![no_main]
{% if false %}
 //
 //  _    _            _ 
 // | |  | |          | |
 // | |__| | ___ _   _| |
 // |  __  |/ _ \ | | | |
 // | |  | |  __/ |_| |_|
 // |_|  |_|\___|\__, (_)
 //               __/ |  
 //              |___/   
 //  _____ _    _      _                                                                 _       
 // |_   _| |_ (_)___ (_)___  __ _   __ __ _ _ _ __ _ ___ ___ __ _ ___ _ _  ___ _ _ __ _| |_ ___ 
 //   | | | ' \| (_-< | (_-< / _` | / _/ _` | '_/ _` / _ \___/ _` / -_) ' \/ -_) '_/ _` |  _/ -_)
 //   |_| |_||_|_/__/ |_/__/ \__,_| \__\__,_|_| \__, \___/   \__, \___|_||_\___|_| \__,_|\__\___|
 //  -                  -      -       -        |___/        |___/                               
 // | |_ ___ _ __  _ __| |__ _| |_ ___| |
 // |  _/ -_) '  \| '_ \ / _` |  _/ -_)_|
 //  \__\___|_|_|_| .__/_\__,_|\__\___(_)
 //               |_|                    
 //
 // Hey! This is a cargo-generate template. You're not meant to clone this repo.
 // Look at the README for instructions on how to install and use cargo-generate.
 //
{% endif %}
{% if framework == "None" %}use {{ bsp }} as bsp;
use bsp::entry;
use bsp::XOSC_CRYSTAL_FREQ;
use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},{% if bsp == "seeeduino_xiao_rp2040" %}
    gpio::PinState,{% endif %}
    pac,
    sio::Sio,
    watchdog::Watchdog,
};

use embedded_hal::digital::OutputPin;

use defmt_rtt as _;
use panic_probe as _;{% if probe %}
use defmt::info;{% endif %}

#[entry]
fn main() -> ! { {% if probe %}
    info!("Program start");{% endif %}
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);
    
    let clocks = init_clocks_and_plls( 
        XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );
    {% if bsp == "rp_pico" %}
    let mut led_pin = pins.led.into_push_pull_output();{% elsif bsp == "seeeduino_xiao_rp2040" %}
    // The RGB LED has a common anode, so pulling the GPIO pins high turns it off.
    let mut r_led = pins.led_red.into_push_pull_output_in_state(PinState::High);
    let mut g_led = pins.led_green.into_push_pull_output_in_state(PinState::High);
    let mut b_led = pins.led_blue.into_push_pull_output_in_state(PinState::High); {% endif %}

    loop { {% if bsp == "rp_pico" %} {% if probe %}
        info!("On!");{% endif %}
        led_pin.set_high().unwrap();
        delay.delay_ms(500);
        {% if probe %}
        info!("Off!");{% endif %}
        led_pin.set_low().unwrap();
        delay.delay_ms(500);{% elsif bsp == "seeeduino_xiao_rp2040" %}
        b_led.set_high().unwrap();
        r_led.set_low().unwrap();
        delay.delay_ms(250);
        r_led.set_high().unwrap();
        g_led.set_low().unwrap();
        delay.delay_ms(250);
        g_led.set_high().unwrap();
        b_led.set_low().unwrap();
        delay.delay_ms(250);{% endif %}
    }
}{% elsif framework == "RTIC" %}#![feature(type_alias_impl_trait)]
#[rtic::app(
    device = {{ bsp }}::hal::pac,
    dispatchers = [PWM_IRQ_WRAP]
)]
mod app {
    use crate::app::gpio::{bank0::Gpio{% if bsp == "rp_pico" %}25{% elsif bsp == "seeeduino_xiao_rp2040" %}16{% endif %}, FunctionSio, PullDown, SioOutput};
    use {{ bsp }}::{
        hal::{clocks, gpio, sio::Sio, watchdog::Watchdog},
        Pins, XOSC_CRYSTAL_FREQ,
    };

    use embedded_hal::digital::StatefulOutputPin;
    use rtic_monotonics::rp2040::prelude::*;
    rp2040_timer_monotonic!(Timer);

    use defmt_rtt as _;
    use panic_probe as _;
    use defmt::info;

    #[shared]
    struct Shared {}

    #[local]
    struct Local {
        led: gpio::Pin<Gpio{% if bsp == "rp_pico" %}25{% elsif bsp == "seeeduino_xiao_rp2040" %}16{% endif %}, FunctionSio<SioOutput>, PullDown>,
    }

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local) { {% if probe %}
        info!("Program start");{% endif %}
        let mut watchdog = Watchdog::new(cx.device.WATCHDOG);
        let sio = Sio::new(cx.device.SIO);

        let _clocks = clocks::init_clocks_and_plls(
            XOSC_CRYSTAL_FREQ,
            cx.device.XOSC,
            cx.device.CLOCKS,
            cx.device.PLL_SYS,
            cx.device.PLL_USB,
            &mut cx.device.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        Timer::start(cx.device.TIMER, &mut cx.device.RESETS);

        let pins = Pins::new(
            cx.device.IO_BANK0,
            cx.device.PADS_BANK0,
            sio.gpio_bank0,
            &mut cx.device.RESETS,
        );

        let led = pins.led{% if bsp == "seeeduino_xiao_rp2040" %}_green{% endif %}.into_push_pull_output();
        
        blink::spawn().unwrap();
        (Shared {}, Local { led })
    }
    
    #[task(local = [led])]
    async fn blink(cx: blink::Context) {
        loop {
            cx.local.led.toggle().unwrap();
            Timer::delay(500.millis()).await;{% if probe %}
            info!("Toggled!");{% endif %}
        }
    }
}{% elsif framework == "Embassy" %}{% if wifi == "Raspberry Pi Pico W" %}
use cyw43_pio::PioSpi;

use embassy_executor::Spawner;
use embassy_rp::{
    bind_interrupts,
    gpio::{Level, Output},
    peripherals::{DMA_CH0, PIO0, PIN_23, PIN_25},
    pio::{InterruptHandler, Pio},
};
use embassy_time::{Duration, Timer};

use static_cell::StaticCell;

use defmt_rtt as _;
use panic_probe as _;{% if probe %}
use defmt::info;{% endif %}

bind_interrupts!(struct Irqs {
    PIO0_IRQ_0 => InterruptHandler<PIO0>;
});

#[embassy_executor::task]
async fn wifi_task(runner: cyw43::Runner<'static, Output<'static, PIN_23>, PioSpi<'static, PIN_25, PIO0, 0, DMA_CH0>>) -> ! {
    runner.run().await
}

#[embassy_executor::main]
async fn main(spawner: Spawner) { {% if probe %}
    info!("Program start");{% endif %}
    let p = embassy_rp::init(Default::default());
    
    let fw = include_bytes!("../wifi_chip_fw.bin");
    let clm = include_bytes!("../wifi_chip_clm.bin");{}

    let pwr = Output::new(p.PIN_23, Level::Low);
    let cs = Output::new(p.PIN_25, Level::High);
    let mut pio = Pio::new(p.PIO0, Irqs);
    let spi = PioSpi::new(
        &mut pio.common,
        pio.sm0,
        pio.irq0,
        cs,
        p.PIN_24,
        p.PIN_29,
        p.DMA_CH0,
    );

    static STATE: StaticCell<cyw43::State> = StaticCell::new();
    let state = STATE.init(cyw43::State::new());
    let (_net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;
    spawner.spawn(wifi_task(runner)).expect("Errored!");

    control.init(clm).await;
    control
        .set_power_management(cyw43::PowerManagementMode::PowerSave)
        .await;

    let delay = Duration::from_secs(1);
    loop { {% if probe %} 
        info!("led on!");{% endif %}
        control.gpio_set(0, true).await;
        Timer::after(delay).await;
        {% if probe %}
        info!("led off!"); {% endif %}
        control.gpio_set(0, false).await;
        Timer::after(delay).await;
    }
}
{% endif %}{% endif %}
