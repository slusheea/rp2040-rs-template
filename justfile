@flash:
    cargo {% if compiler == "nightly" %}+nightly {% endif %}run --target thumbv6m-none-eabi

@release:
    cargo {% if compiler == "nightly" %}+nightly {% endif %}run --release --target thumbv6m-none-eabi

@clippy:
    cargo {% if compiler == "nightly" %}+nightly {% endif %}clippy -- -W clippy::pedantic -W clippy::nursery{% if probe %}

@debug:
    probe-rs debug --chip RP2040 --protocol swd{% endif %}
