# Embedded rust template for rp2040 boards

## Support
- ✅ Supported
- ⚙️ Working on it
- 🟨 Planned
- 🟧 Maybe
- ❌ Not supported

|Board|No framework|Embassy|RTIC|
|--|--|--|--|
|Raspberry Pi Pico|✅|🟨|✅|
|Raspberry Pi Pico W|🟨|✅|❌|
|Seedstudio XIAO RP2040|✅|🟧|✅|
|Picoboy|🟧|🟧|🟧|
|Other|❌|❌|❌|

I don't plan on getting more boards any time soon, so other boards might not get support.

Merge requests welcome.

## How to create a new project:
```bash
cargo generate gl:slusheea/rp2040-template
```
or add this to your cargo-generate.toml:
```toml
# Will create a new project if you run `cargo generate pico`
[favorites.pico]
git = "https://gitlab.com/slusheea/rp2040-template"
```

(make sure you have [cargo-generate](https://crates.io/crates/cargo-generate) installed)

## How to flash your code
The template includes a [`justfile`](https://crates.io/crates/just) that'll make flashing your code easy. Run
```bash
just flash
```
to flash your code to your board. 

Otherwise you can use:
```bash
cargo run --release --target=thumbv6m-none-eabi
```

## Extra info
- If you haven't done the setup for embedded yet, you'll need to run the following: 
```bash
rustup self update
rustup update stable
rustup target add thumbv6m-none-eabi
cargo install flip-link
cargo install elf2uf2-rs --locked # <- if you intend to use USB flashing
# probe-rs installation instructions: https://probe.rs/docs/getting-started/installation/ # <- if you intend to use probe flashing
```
- If `cargo install elf2uf2-rs --locked` fails, you might need the `eudev-libudev-devel` package (on void linux) or `libudev-dev` (on ubuntu based systems).
- If compiling fails, you might need to install `avr-libc` (on void and ubuntu based systems).
- If you're using Nix or NixOS you can use the following flake: https://gitlab.com/slusheea/rp2040-template/-/snippets/3725973

## License
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
